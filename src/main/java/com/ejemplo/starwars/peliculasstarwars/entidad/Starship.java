package com.ejemplo.starwars.peliculasstarwars.entidad;

import java.util.List;

public class Starship {
	
	private List<String> starships;

	public List<String> getStarships() {
		return starships;
	}

	public void setStarships(List<String> starships) {
		this.starships = starships;
	}

}
