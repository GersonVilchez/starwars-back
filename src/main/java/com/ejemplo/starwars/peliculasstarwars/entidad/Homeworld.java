package com.ejemplo.starwars.peliculasstarwars.entidad;

import java.util.List;

public class Homeworld {

	private List<String> homeworld;

	public List<String> getHomeworld() {
		return homeworld;
	}

	public void setHomeworld(List<String> homeworld) {
		this.homeworld = homeworld;
	}
	
}
