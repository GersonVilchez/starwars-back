package com.ejemplo.starwars.peliculasstarwars.entidad;

import java.util.List;

public class Vehicle {
	private List<String> vehicles;

	public List<String> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<String> vehicles) {
		this.vehicles = vehicles;
	}
}
