package com.ejemplo.starwars.peliculasstarwars.entidad;

import java.util.List;

public class Film {
	
	private List<String> films;

	public List<String> getFilms() {
		return films;
	}

	public void setFilms(List<String> films) {
		this.films = films;
	}
	
}
