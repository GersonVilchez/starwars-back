package com.ejemplo.starwars.peliculasstarwars.entidad;

import java.util.List;

public class Species {
	
	private List<String>species;

	public List<String> getSpecies() {
		return species;
	}

	public void setSpecies(List<String> species) {
		this.species = species;
	}

}
