package com.ejemplo.starwars.peliculasstarwars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeliculasStarwarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeliculasStarwarsApplication.class, args);
	}

}
