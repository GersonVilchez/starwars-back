package com.ejemplo.starwars.peliculasstarwars.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.ls.LSResourceResolver;

import com.ejemplo.starwars.peliculasstarwars.entidad.General;
import com.ejemplo.starwars.peliculasstarwars.entidad.CharacterDetail;
import com.ejemplo.starwars.peliculasstarwars.entidad.CharacterFilm;
import com.ejemplo.starwars.peliculasstarwars.entidad.Results;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/prueba")
@CrossOrigin("*")
public class StarWarsController {
	
	@GetMapping("/films")
	public Object getAllFilms() {
		Object obj;
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<General> generalResponse = restTemplate.getForEntity("https://swapi.dev/api/films/", General.class);
		General avatar = generalResponse.getBody();
		return avatar;
	}

	@GetMapping("/detail")
	public Object getDetail(@RequestHeader String filmName) {
		Object obj;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<General> generalResponse = restTemplate.getForEntity("https://swapi.dev/api/films/", General.class);
		General avatar = generalResponse.getBody();

		if (filmName.equals("1")) {
			for (int i = 0; i <= avatar.getResults().size(); i++) {
				if (avatar.getResults().get(i).getEpisode_id() == 1) {
					return generalResponse.getBody().getResults().get(i);
				}
			}
		}
		if (filmName.equals("2")) {
			for (int i = 0; i <= avatar.getResults().size(); i++) {
				if (avatar.getResults().get(i).getEpisode_id() == 2) {
					return generalResponse.getBody().getResults().get(i);
				}
			}
		}
		if (filmName.equals("3")) {
			for (int i = 0; i <= avatar.getResults().size(); i++) {
				if (avatar.getResults().get(i).getEpisode_id() == 3) {
					return generalResponse.getBody().getResults().get(i);
				}
			}
		}
		if (filmName.equals("4")) {
			for (int i = 0; i <= avatar.getResults().size(); i++) {
				if (avatar.getResults().get(i).getEpisode_id() == 4) {
					return generalResponse.getBody().getResults().get(i);
				}
			}
		}
		if (filmName.equals("5")) {
			for (int i = 0; i <= avatar.getResults().size(); i++) {
				if (avatar.getResults().get(i).getEpisode_id() == 5) {
					return generalResponse.getBody().getResults().get(i);
				}
			}
		}
		if (filmName.equals("6")) {
			for (int i = 0; i <= avatar.getResults().size(); i++) {
				if (avatar.getResults().get(i).getEpisode_id() == 6) {
					return generalResponse.getBody().getResults().get(i);
				}
			}
		}
		return "--";
	}

	@GetMapping("/character")
	public Object getAllCharacter(@RequestHeader String requestUrl) {
		Object obj;
		requestUrl = "https" + requestUrl.substring(4);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CharacterDetail> generalResponse = restTemplate.getForEntity(requestUrl, CharacterDetail.class);
		CharacterDetail characterDetail = generalResponse.getBody();
		return characterDetail;
	}
	
	@GetMapping("/filmxcharacter")
	public Object getAllFilmsxCharacter(@RequestHeader String requestUrl2) {
		Object obj;
		requestUrl2 = "https" + requestUrl2.substring(4);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CharacterFilm> generalResponse = restTemplate.getForEntity(requestUrl2, CharacterFilm.class);
		CharacterFilm characterFilm = generalResponse.getBody();
		return characterFilm;
	}

}
